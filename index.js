(links => {
	
	window.addEventListener("load", () => {
		document.body.removeAttribute("unresolved");
	});

	// Set a reference to the <aside> element
	const aside = document.querySelector("aside");
	
	// Set a reference to the <template> element
	const template = document.querySelector("template");
	
	// Goes trough all the links
	Object.keys(links).forEach(title => {
		
		// Gets the template content
		const templateContent = document.querySelector("template").content;
		
		// Gets the anchor inside the template
		const a = templateContent.querySelector("a");
		
		// Sets aria-label, svg icon and href on the anchor
		a.setAttribute("aria-label", title);
		a.querySelector("use").setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", "icons.svg#" + title.toLowerCase());
		a.setAttribute("href", links[title]);
		a.querySelector("span").textContent = title;
		
		// Appendhs the anchor to the <aside> element
		aside.appendChild(document.importNode(templateContent, true));
	});

	// Removes the template from the DOM
	template.parentNode.removeChild(template);

})({
	Twitter: "https://twitter.com/guidorugo",
	GMail: "mailto:guido.rugo@gmail.com",
	LinkedIn: "https://linkedin.com/in/grugo",
	Bitbucket: "https://bitbucket.org/guidorugo/",
	CV: "./cv.pdf",
	GPG: "./guido.pem"
});